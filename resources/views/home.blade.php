@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Sale</div>

                <div class="card-body">
                    @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{Session::get('success')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                    <div>
                    <form action="{{ route('product') }}" method="post">
                    {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="product_1" class="col-md-4 col-form-label text-md-right">{{ __('Medium Cup') }}</label>

                            <div class="col-md-6">
                                <input id="product_1" type="number" min="0" value="0" class="form-control @error('password') is-invalid @enderror" name="product_1" required>

                                @error('product_1')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="product_2" class="col-md-4 col-form-label text-md-right">{{ __('Large Cup') }}</label>

                            <div class="col-md-6">
                                <input id="product_2" type="number" min="0" value="0" class="form-control @error('password') is-invalid @enderror" name="product_2" required>

                                @error('product_2')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
