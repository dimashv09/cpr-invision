@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Report</div>

                <div class="card-body">
                    <div class="hscroll">
                        <table id="reportTable" class="table table-bordered table-striped">
                            <thead>
                                <tr style="text-align:center">
                                    <th>Date</th>
                                    <th>Submited By</th>
                                    <th>Medium Cup</th>
                                    <th>Large Cup</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $data)
                                <tr>
                                    <td>{{ $data->created_at }}</td>
                                    <td>{{ $data->user->name }}</td>
                                    <td>{{ $data->product_1 }}</td>
                                    <td>{{ $data->product_2 }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
