<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductLog;

class ReportController extends Controller
{
    protected $request;
    protected $product_log;

    public function __construct( Request $request, ProductLog $product_log )
    {
        $this->product_log      = $product_log;
        $this->request          = $request;
    }
    
    public function index()
    {
        $product_logs = ProductLog::all();

        return view('report', [
            "datas" => $product_logs
        ]);
    }
}
