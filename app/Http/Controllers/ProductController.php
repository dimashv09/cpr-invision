<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ProductLog;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    protected $request;
    protected $product_log;

    public function __construct( Request $request, ProductLog $product_log )
    {
        $this->product_log      = $product_log;
        $this->request          = $request;
    }

    public function index()
    {
        return view('home');
    }

    public function productLog()
    {
        $this->validate($this->request, [
            'product_1' => 'required',
            'product_2' => 'required'
        ]);
        
        $user_id    = Auth::user()->id;
        $product_1  = $this->request->product_1;
        $product_2  = $this->request->product_2;

        $this->product_log->create([
            'user_id'   => $user_id,
            'product_1' => $product_1,
            'product_2' => $product_2,
        ]);
        
        session()->flash('success', 'Data successfully added!');
        return redirect()->route('product.index');
    }
}
