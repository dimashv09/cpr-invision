<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::post('/loginPin', 'Auth\LoginController@loginPin')->name('loginPin');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/product', 'ProductController@index')->name('product.index');
Route::post('/productLog', 'ProductController@productLog')->name('product');

Route::get('/report', 'ReportController@index')->name('report');
